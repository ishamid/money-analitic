import React, { Component } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import "./App.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/custom.css';

const loading = () => (

  <div className="animated fadeIn pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

// // Containers
const Homepage = React.lazy(() => import("./views/Homepage"));

// // Pages
// const Login = React.lazy(() => import("./views/Pages/Login"));
// const LoginInaPort = React.lazy(() => import("./views/Pages/LoginInaPort"));
// const ForgotPass = React.lazy(() => import("./views/Pages/ForgotPass"));
// const Register = React.lazy(() => import("./views/Pages/Register"));
// const Page404 = React.lazy(() => import("./views/Pages/Page404"));
// const Page500 = React.lazy(() => import("./views/Pages/Page500"));

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading()}>
          <Switch>
            {/* <Route
              exact
              path="/login"
              name="Login Page"
              render={(props) => <Login {...props} />}
            /> */}
            <Route
              path="*"
              name="Beranda"
              render={(props) => <Homepage {...props} />}
            />
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;