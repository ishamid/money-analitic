import React from "react";

const Core = React.lazy(() => import("../views/core/Core"));
const Income = React.lazy(() => import("../views/income/Income"));
const Spend = React.lazy(() => import("../views/spend/Spend"));

const routes = [
    { path: "/", exact: true, name: "Beranda" },
    {
        path: "/",
        component: Core,
        exact: true
    },
    {
        path: "/home",
        component: Core,
        exact: true
    },
    {
        path: "/income",
        component: Income,
        name: Income,
        exact: true
    },
    {
        path: "/spend",
        component: Spend,
        name: Spend,
        exact: true
    },

]

export default routes;
