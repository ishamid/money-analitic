import React, { Component } from "react";
import { Row, Col, Card, Table, Button, InputGroup, InputGroupAddon, InputGroupText, Input } from "reactstrap";
import { Link } from "react-router-dom"

class Income extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            product: [],
            selectedOption: null
        };
    }

    componentWillMount() {
    }
    render() {
        return (
            <>
                <Row>
                    <Col sm={12} xl={12}>
                        <p className="font-0">Pemasukan</p>
                    </Col>
                    <Col xs={12} xl={12}>
                        <Card>
                            <Row className="my-3">
                                <Col className="p-0">
                                    <Button size="sm" className="rounded" color="primary">Tambah</Button>
                                </Col>
                                <Col className="p-0">
                                    <InputGroup size="sm">
                                        <Input placeholder="username" />
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText><i className="fas fa-search"></i></InputGroupText>
                                        </InputGroupAddon>
                                    </InputGroup>
                                </Col>
                            </Row>
                            <Table className="table-borderless table-binding" striped>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>Sumber Pemasukan</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Mark</td>
                                        <td>Otto</td>
                                        <td>@mdo</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>Jacob</td>
                                        <td>Thornton</td>
                                        <td>@fat</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Larry</td>
                                        <td>the Bird</td>
                                        <td>@twitter</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Income;