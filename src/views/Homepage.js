import React, { Component } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import routes from "../routes/Routes";
import FadeIn from 'react-fade-in';
import "../assets/fonts/all.css"
import { Col, Row, Button, NavbarBrand, Form, FormGroup, Label, Input } from 'reactstrap';
import { Container } from "reactstrap";

const navLinks = [
    { url: '/home', name: 'Beranda', icon: 'fas fa-hotel' },
    { url: '/income', name: 'Pemasukan', icon: 'fas fa-money-check-alt' },
    { url: '/spend', name: 'Pengeluaran', icon: 'fas fa-comment-dollar' },
];

const authorLinks = [
    { url: '/about-us', name: 'Pembukuan', icon: 'fas fa-scroll' },
    { url: '/projects', name: 'Profil', icon: 'fas fa-user-alt' },
    { url: '/services', name: 'Petunjuk', icon: 'fas fa-question' },
];

class Homepage extends Component {
    constructor() {
        super();
        this.state = {
            style: "menu",
            menuStatus: "open"
        };
        this.handleClick = this.handleClick.bind(this);
    };

    loading = () => (
        <div className="animated fadeIn pt-1 text-center">
            <div className="sk-spinner sk-spinner-pulse"></div>
        </div>
    );

    componentDidMount() {
        window.scrollTo(0, 0)
    }

    handleClick() {
        switch (this.state.menuStatus) {
            case "open":
                this.setState({
                    menuStatus: "close",
                    style: "menu active"
                });
                break;
            case "close":
                this.setState({
                    menuStatus: "open",
                    style: "menu"
                });
                break;
        }
    }

    render() {
        return (
            <>
                <div className="app-body">
                    <main className="main">
                        <Row className="section-pages">
                            <Col lg={2} xs={12} className={`side-bar ${this.state.style} p-0`}>
                                <Row className="align-items-center">
                                    <NavbarBrand href="/" className="mr-auto">moneyster</NavbarBrand>
                                    <Button onClick={this.handleClick} color="light" className="expand-btn">
                                        <i class="fas fa-angle-double-left"></i>
                                    </Button>
                                </Row>
                                <hr />
                                <ul>
                                    {navLinks.map(({ url, name, icon }, idx) => (
                                        <li key={idx}>
                                            <a href={url}><i className={icon}></i>{name}</a>
                                        </li>
                                    ))}
                                </ul>
                                <hr />
                                <ul>
                                    {authorLinks.map(({ url, name, icon }, idx) => (
                                        <li key={idx}>
                                            <a href={url}><i className={icon}></i>{name}</a>
                                        </li>
                                    ))}
                                </ul>
                            </Col>
                            <Col lg={10} xs={12} className="content">
                                <FadeIn transitionDuration={1300}>
                                    <Container fluid={true}>
                                        <Switch>
                                            {routes.map((route, idx) => {
                                                return route.component ? (
                                                    <Route
                                                        key={idx}
                                                        path={route.path}
                                                        exact={route.exact}
                                                        name={route.name}
                                                        render={(props) => <route.component {...props} />}
                                                    />
                                                ) : null;
                                            })}
                                            <Redirect
                                                from="/"
                                                to={{
                                                    pathname: "/home"
                                                }}
                                            />
                                        </Switch>
                                    </Container>
                                </FadeIn>
                            </Col>
                        </Row>
                    </main>
                </div>
            </>
        );
    }
}

export default Homepage;