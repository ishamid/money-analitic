import React, { Component } from "react";
import { Row, Col, Card, Table, Button, InputGroup, InputGroupAddon, InputGroupText, Input } from "reactstrap";
import { Link } from "react-router-dom"

class Spend extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            product: [],
            selectedOption: null
        };
    }

    componentWillMount() {
    }
    render() {
        return (
            <>
                <Row>
                    <Col sm={12} xl={12}>
                        <p className="font-0">Pemasukan</p>
                    </Col>
                    <Col xs={12} xl={12}>
                        <Card>
                            <Row className="my-3">
                                <Col className="p-0">
                                    <Button size="sm" className="rounded" color="info">Tambah</Button>
                                </Col>
                                <Col className="p-0">
                                    <InputGroup size="sm">
                                        <Input placeholder="username" />
                                        <InputGroupAddon addonType="prepend">
                                            <InputGroupText><i className="fas fa-search"></i></InputGroupText>
                                        </InputGroupAddon>
                                    </InputGroup>
                                </Col>
                            </Row>
                            <Table className="table-borderless table-binding" striped>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tanggal</th>
                                        <th>Pengeluaran</th>
                                        <th>QTY</th>
                                        <th>Harga</th>
                                        <th>Jumlah</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>22/06/2020</td>
                                        <td>Beli Shampoo</td>
                                        <td>Botol</td>
                                        <td>23,000</td>
                                        <td>1</td>
                                        <td>23,000</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Card>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Spend;