import React, { Component } from "react";
import { Row, Col, Card } from "reactstrap";
import { Link } from "react-router-dom"

class Core extends Component {
    constructor(props) {
        super(props);
        this.state = {
            index: 0,
            product: [],
            selectedOption: null
        };
    }

    componentWillMount() {
    }
    render() {
        return (
            <>
                <Row>
                    <Col sm={12} xl={12}>
                        <p className="font-0">Beranda</p>
                    </Col>
                    <Col xs={12} xl={8}>
                        <Card>
                            PART CHART
                        </Card>
                    </Col>
                    <Col xs={12} xl={4}>
                        <Card>
                            NEW ACTION
                        </Card>
                    </Col>
                    <Col xs={12} xl={4}>
                        <Card>
                            <Link to={{ pathname: '/income' }}>
                                INCOME
                            </Link>
                        </Card>
                    </Col>
                    <Col xs={12} xl={4}>
                        <Card>
                            SPEND
                        </Card>
                    </Col>
                    <Col xs={12} xl={4}>
                        <Card>
                            SAVING
                        </Card>
                    </Col>
                </Row>
            </>
        );
    }
}

export default Core;